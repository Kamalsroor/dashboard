<?php
    return [

        "apiOperation"          => "CREATE_CHECKOUT_SESSION",
        "currency"              => "SAR",
        "ApiUrl"                => "https://ap-gateway.mastercard.com/api/rest/version/58",
        "ApiUrlTest"            => "https://test-gateway.mastercard.com/api/rest/version/59",
        // "MeezaJSSandBoxUrl"     => "https://upgstaging.egyptianbanks.com:3006/js/Lightbox.js",
        // "MeezaJSLiveBoxUrl"     => "https://upg.egyptianbanks.com:2008/INVCHOST/js/Lightbox.js",
        "checkOutSandBoxUrl"    => "https://test-gateway.mastercard.com/checkout/version/59/checkout.js",
        "checkOutLiveUrl"       => "https://qnbalahli.gateway.mastercard.com/checkout/version/58/checkout.js",

    ];
