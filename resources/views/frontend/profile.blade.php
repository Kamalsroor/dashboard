
<x-front-layout :title="trans('dashboard.home')" :breadcrumbs="['dashboard.home']">
<div class="container-fluid p-0 m-0 profile-bg" >
<div class="container py-4 ">
        <div class="row align-items-start py-2" style="background-color: #cccccce6;">
            <div class="col-lg-5 col-md-12 text-center justify-content-center">
                <div >
                    <img  src="{{asset('front/img/5.png')}}" style="width: 200px;border-radius: 50%;" alt="profile-img">
                </div>
                <div style="font-size: 20px;" class="my-3 d-flex justify-content-start align-items-start">
                    <img src="{{asset('front/img/riyal.png')}}" alt="coins">
                    <div style="width: 100%;">
                        <div class="d-flex justify-content-between" style="width: 100%;">
                            <p class="color-black"> رصيد النقاط</p>
                            <p class="color-black " >125</p>
                        </div>
                        <div class="d-flex justify-content-between" style="width: 100%;">
                            <p class="color-black " >ريال</p>
                            <p class="color-black">100</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center align-items-end">
                    <img src="{{asset('front/img/smallgoldencard.png')}}" alt="client-type" style="width: 90%;">
                    <div class="pb-2" style="width: 70%;position:absolute;font-weight: 700;text-shadow: 1px 1px black;">
                        <p class="mb-0">client Name</p>
                        <div class="d-flex justify-content-between" >
                            <p class="mb-0" >DATE</p>
                            <p class="mb-0" >1234567890</p>
                        </div>
                     
                        <div class="d-flex justify-content-between" >
                            <p class="mb-0" >ذهبى</p>
                            <p class="mb-0" >gold</p>
                        </div>

                    </div>
                    
                        
                    
                </div>

                <div class="mt-4">
                    <a class="btn btn-primary" id="toggel-profile" style="cursor: pointer;">تعديل البيانات</a>
                </div>
            </div>
            <div class="col-lg-7">
                <h4 class="text-right color-black" style="font-weight: 600;">البيانات الشخصية</h4>

                <div id="profile">
                <table class="table table-striped color-black">
                    <tbody>
                        <tr>
                            <th class="color-black text-center" scope="row">الأسم</th>
                            <td class="color-black text-center">احمد خالد السيد</td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">رقم الهوية</th>
                            <td class="color-black text-center">12555847</td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">تاريخ انتهاء الهوية</th>
                            <td class="color-black text-center">12555847</td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">تاريخ انتهاء رخصة القيادة</th>
                            <td class="color-black text-center">01/06/2023</td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">تاريخ الميلاد</th>
                            <td class="color-black text-center">01/04/1992</td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">الجنسية</th>
                            <td class="color-black text-center">مصرى</td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">رقم الجوال</th>
                            <td class="color-black text-center">05015477730</td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">البريد الالكترونى</th>
                            <td class="color-black text-center">ahmedkhaled@gmail.com</td>

                        </tr>
                        
                        
                    
                        <tr>
                            <th class="color-black text-center" scope="row">النوع</th>
                            <td class="color-black text-center">ذكر</td>

                        </tr>

                        <tr>
                            <th class="color-black text-center" scope="row">العنوان</th>
                            <td class="color-black text-center">25 street 485 gamal abd el naser</td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">صندوق البريد</th>
                            <td class="color-black text-center">24 street 502 shatby </td>

                        </tr>

                       
                        
                       
                    </tbody>
                </table>
                </div>

                <div id="update-profile" class="d-none">
                @include('frontend.profile.form')
                </div>


            </div>
        </div>


    </div>
    </div>
</x-front-layout>
