    
    <form action="#">
    <table class="table table-striped color-black">
                    
                    <tbody>
                        

                        <tr>
                            <th class="color-black text-center" scope="row">كلمة السر</th>
                            <td class=" color-black text-center">
                                <div class="form-group">
                                    <label class="color-black" style="font-size: 16px;">ادخل كلمة السر الجديدة</label>
                                    <input type="text" class="color-black form-control" name="name" value="احمد خالد السيد">
                                </div>
                                <div class="form-group">
                                    <label class="color-black" style="font-size: 16px;">تأكيد كلمة السر</label>
                                    <input type="text" class="color-black form-control" name="name" value="احمد خالد السيد">
                                </div>
                            </td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">رقم الهوية</th>
                            <td class="color-black text-center"><input type="text" class="color-black form-control" name="id-number" value="احمد خالد السيد"></td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">تاريخ انتهاء الهوية</th>
                            <td class="color-black text-center"><input type="date" class="color-black form-control" name="date-id-end" ></td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">تاريخ انتهاء رخصة القيادة</th>
                            <td class="color-black text-center"><input type="date" class="color-black form-control" name="date-licence-end" ></td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">تاريخ الميلاد</th>
                            <td class="color-black text-center"><input type="date" class="color-black form-control" name="date-of-birth" ></td>

                        </tr>
                        <tr>
                            <th class="color-black text-center">الجنسية</th>
                            <td class="color-black text-center">
                            <select name="nationality" class="form-control color-black text-center">
                            <option class="color-black text-center" value="egypt"  >مصر</option>
                            <option class="color-black text-center" value="saudia"  >السعودية</option>
                            <option class="color-black text-center" value="yamen"  >اليمن</option>
                            </select>
                            </td>
                            
                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">رقم الجوال</th>
                            <td class="color-black text-center"><input type="text" class="color-black form-control" name="mobile-number" value="01558945898"></td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">البريد الالكترونى</th>
                            <td class="color-black text-center"><input type="text" class="color-black form-control" name="email" value="ahmedkhaledelsayed@gmail.com"></td>

                        </tr>
                        
                        
                    
                        <tr>
                            <th class="color-black text-center" scope="row">النوع</th>
                            <td>
                                <select class="text-center color-black form-control" name="sex" >
                                    <option class="color-black" value="male">ذكر</option>
                                    <option class="color-black" value="female">انثى</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <th class="color-black text-center" scope="row">العنوان</th>
                            <td class="color-black text-center"><input type="text" class="color-black form-control" name="adress" value="24 street 503 shatby"></td>

                        </tr>
                        <tr>
                            <th class="color-black text-center" scope="row">صندوق البريد</th>
                            <td class="color-black text-center"><input type="text" class="color-black form-control" name="mail-box" value="24 street 503 shatby"></td>

                        </tr>

                       
                        
                       
                    </tbody>
                </table>
                <button type="submit" class="btn btn-primary">تأكيد البيانات</button>
    </form>