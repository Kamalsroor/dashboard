<footer class="footer d-lg-block " style="background: no-repeat linear-gradient( 359deg, #0081de 5%, #001338 100%);;">
    <div class="container-fluid px-0">
       
        <div class="row py-2  mx-0 px-0">


            <div class="col-lg-4 col-md-12 text-center my-1">
                <img src="{{asset('front/img/logo-edited-.png')}}" class="logo-footer" alt="logo">
                    <div class="my-2">
                        <p style="font-size:20px;color:#ffc107;">يسعدنا خدمتكم من خلال تطبيقنا</p>
                    </div>
                <div class="container-fluid">
                        <div class="row justify-content-center px-3 text-center">
                            <div class="col-3 mx-1 px-0">
                                <a href="" class="px-0" style="width:50%;"><img style="border: white solid 1px;border-radius: 15px;width:100%;" class="w-100" src="{{asset('front/img/google.jpg')}}" alt=""></a>
                            </div>
                            <div class="col-3 mx-1 px-0">
                                <a href="" class="px-0" style="width:50%;"><img style="border: white solid 1px;border-radius: 15px;" class="w-100" src="{{asset('front/img/app-store.jpg')}}" alt=""></a>
                            </div>
                            <div class="col-3 mx-1 px-0">
                                <a href="" class="px-0" style="width:50%;"><img style="border: white solid 1px;border-radius: 15px;" class="w-100" src="{{asset('front/img/app-gelery.jpg')}}" alt=""></a>
                            </div>

                        </div>
                </div>


            </div>
            <div class="col-lg-4 col-md-12 text-center d-flex align-items-center justify-content-center">
                <div class=" text-center " style="width: fit-content;height:fit-content;">
                    <p class="mb-0" style="font-size:20px;">العنوان</p>
                    <p class="mb-0" style="font-size:20px;">الإدارة العامة - الرياض</p>
                    <p  style="font-size:20px;">شارع الكتاب - حى الملك عبد العزيز</p>
                    <a  href="tel:996920026600" class="btn btn-danger btn-lg btn-block mx-0" style="font-weight: 700;font-size:30px;">920026600 <i class="fas fa-phone-volume"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-md-12 text-center my-1 d-flex align-items-center justify-content-center text-center m-auto" style="flex-direction: column;">
                    <div style="width: fit-content;">
                    <a style="font-size: 35px;margin-right:3px;" href="https://www.facebook.com/abudiyabsa"><i class="fab fa-facebook-f face-book"></i></a>
                    <a style="font-size: 35px;margin-right:3px;" href="https://twitter.com/abudiyabsa"><i class="fab fa-twitter twitter"></i></a>
                    <a style="font-size: 35px;margin-right:3px;" href="https://api.whatsapp.com/send?phone=00966557492493#xd_co_f=MjA3YTM5MmZkYzdkMDhjODcyMjE2MTIzNDczNDQ5MjQ=~"><i class="fab fa-whatsapp whatsapp"></i></a>
                    <a style="font-size: 35px;margin-right:3px;" href="https://www.youtube.com/channel/UC3gtVL9XMxqGPXNB6B6s6NA?reload=9"><i class="fab fa-youtube youtube"></i></a>
                    <a style="font-size: 35px;margin-right:3px;" href="https://www.instagram.com/abudiyabsa/"><i class="fab fa-instagram instagram"></i></a>
                    <a style="font-size: 35px;margin-right:3px;" href="https://www.linkedin.com/company/abu-diyab-rent-a-car/"><i class="fab fa-linkedin-in likedin"></i></a>
                    
                    </div>
                <div style="width: fit-content;height: fit-content;">
                <div class="my-2">
                    <p style="font-size:20px;color:#ffc107;">أبق على تواصل معنا من خلال أشتراك فى نشرتنا البريدية</p>
                </div>
                <div class="my-2">
                    <div class="form-group row">
                        <div class="col-lg-8 col-8">
                            <input type="mail" class="form-control" placeholder="أدخل إميلك">
                        </div>
                        <div class="col-lg-4 col-4 ">
                            <a href="#" class="btn btn-danger btn-block ">اشترك معنا</a>
                        </div>
                    </div>
                </div>
                </div>
                
                

            </div>
           
                
            
            
        </div>
        <div class="row m-0 px-0" style="background-color: #8c8c8c36;">
                <div class="col-12">
                    <p class="text-center mb-0 py-3">جميع الحقوق محفوظة لشركة <span href="index.php">أبو ذياب لتأجير
                            السيارات</span> © 2021</p>
                </div>
        </div>
        

    </div>
</footer>